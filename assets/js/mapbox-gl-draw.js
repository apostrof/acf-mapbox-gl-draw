/**
 * Custom Mapbox JS
 */

/**
 * Create the custom map
 *
 * @param id str (acf field if)
 * @param center_lat int
 * @param center_lng int
 * @param address str
 * @param zoom int
 * @param styles str
 * @param enable_nav_control bool
 */
function create_map_draw(id, collection, center_lat, center_lng, address, zoom, styles, enable_nav_control) {
    // bail early if Mapbox required JS is not available
    if (typeof mapboxgl === 'undefined') {
        console.error('\'mapboxgl\' object not found, make sure you included the mapbox-gl-js library (go to https://docs.mapbox.com/mapbox-gl-js/overview/)')
        return;
    }
    
    // bail early if Mapbox draw required JS is not available
    if (typeof MapboxDraw === 'undefined') {
        console.error('\'MapboxDraw\' object not found, make sure you included the mapbox-gl-draw library (go to https://github.com/mapbox/mapbox-gl-draw)')
        return;
    }
    
    // bail early if Mapbox geocoder required JS is not available
    if (typeof MapboxGeocoder === 'undefined') {
        console.error('\'MapboxGeocoder\' object not found, make sure you included the mapbox-gl-geocoder library (go to https://github.com/mapbox/mapbox-gl-geocoder)')
        return;
    }

    try {

        let field_id = jQuery('#' + id);

        // Create the map coordinates using the values from the form or from the user's selected location
        let map = new mapboxgl.Map({
            container: 'map_draw_' + id,
            zoom: (zoom) ? zoom : '12',
            center: [center_lng, center_lat],
            style: 'mapbox://styles/mapbox/' + styles
        });
        
        // Create Geocoder control to search for places using Mapbox Geocoding API
        let geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            placeholder: address,
            marker: false,
            // mapboxgl: mapboxgl,
            collapsed: true
        });

        // Create MapboxDraw control to paint lines, points and polygons on the map
        let draw = new MapboxDraw({
            displayControlsDefault: false,
            controls: {
                polygon: true,
                line_string: true,
                point: true,
                trash: true
            }
        });

        let el = document.createElement('div');
        el.className = 'marker';

        let map_marker = new mapboxgl.Marker(el, {
            anchor: 'bottom',
            draggable: true
        })
        
        // Add the navigation control if it is set to be enabled ?>
        if (enable_nav_control) {
            let navControl = new mapboxgl.NavigationControl();
            map.addControl(navControl, 'top-left');
        }

        // Add the geocoder control
        map.addControl(geocoder)

        // Add the MapboxDraw control
        map.addControl(draw);
        
        map.once('load', function(e) {
            // Add saved collection to map
            if (collection) {
                draw.set(JSON.parse(collection));
                updateCollection(field_id, draw, map_marker, map)
            }
            // Trigger map resize after map is loaded (needed to guarantee that canvas fits the container)
            map.resize();
        });
        // Update center after map moving
        map.on('moveend', function(e) {
            updateCenter(field_id, map.getCenter())
        });
        // Update zoom after map zooming
        map.on('zoom', function(e) {
            updateZoom(field_id, map.getZoom())
        });
        // Update data after creating a geometry
        map.on('draw.create', function() {
            updateCollection(field_id, draw, map_marker, map)
        });
        // Update data after deleting a geometry
        map.on('draw.delete', function() {
            deleteCollection(field_id, draw)
        });
        // Update data after editing a geometry
        map.on('draw.update', function() {
            updateCollection(field_id, draw)
        });
        // Update address after geocoder search result
        geocoder.on('result', function(e) {
            updateAddress(field_id, e.result.place_name)
            updateCenter(field_id, e.result.center)
            console.log('result', e.result)
        });
        // Update data after marker dragging
        map_marker.on('dragend', function() {
            updateCollection(field_id, draw, map_marker, map, true)
        })

    } catch (error) {
        // Log important error message
        console.error('[ACF Mapbox draw]', error.message);
    }
}

/**
 * Update map data and collection hidden input
 *
 * @param field_id jQuery object
 * @param draw MapboxDraw instance
 * @param marker mapboxgl.Marker instance
 * @param map mapboxgl.Map instance
 * @param afterdrag boolean
 */
function updateCollection(field_id, draw, marker, map, afterdrag) {
    let hidden_input_collection = jQuery('.input-collection', field_id);
    let collection = draw.getAll();
    let feature = collection.features[collection.features.length - 1];
    
    if (feature.geometry.type === 'Point') {
        if (afterdrag) {
            let lngLat =  marker.getLngLat();
            feature.geometry.coordinates = [lngLat.lng, lngLat.lat];
            
        } else {
            marker.setLngLat(feature.geometry.coordinates).addTo(map);
        }
        updateCenter(field_id, marker.getLngLat());
    } else {
        marker.remove()
    }
    
    collection.features = [feature];

    if (hidden_input_collection.length > 0 && feature) {
        hidden_input_collection.val(JSON.stringify(collection))
    }

    draw.set(collection);

    console.log(collection)
}

/**
 * Delete map data and collection hidden input
 *
 * @param field_id jQuery object
 * @param draw MapboxDraw instance
 */
function deleteCollection(field_id, draw) {
    let hidden_input_collection = jQuery('.input-collection', field_id);

    draw.deleteAll()

    if (hidden_input_collection.length > 0) {
        hidden_input_collection.val('')
    }
}

/**
 * Update coordinates hidden inputs
 *
 * @param field_id jQuery object
 * @param center array
 */
function updateCenter(field_id, center) {
    let hidden_input_lat = jQuery('.input-lat', field_id);
    let hidden_input_lng = jQuery('.input-lng', field_id);

    if (hidden_input_lat.length > 0 && center.lat) {
        hidden_input_lat.val(center.lat);
    }
    if (hidden_input_lng.length > 0 && center.lng) {
        hidden_input_lng.val(center.lng)
    }
}

/**
 * Update zoom hidden input
 *
 * @param field_id jQuery object
 * @param zoom integer
 */
function updateZoom(field_id, zoom) {
    let hidden_input_zoom = jQuery('.input-zoom', field_id);

    if (hidden_input_zoom.length > 0 && zoom) {
        hidden_input_zoom.val(zoom)
    }
}

/**
 * Update address hidden input
 *
 * @param field_id jQuery object
 * @param address string
 */
function updateAddress(field_id, address) {
    let hidden_input_address = jQuery('.input-address', field_id);

    if (hidden_input_address.length > 0 && address) {
        hidden_input_address.val(address)
    }
}