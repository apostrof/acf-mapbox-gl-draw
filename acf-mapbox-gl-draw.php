<?php

/*
Plugin Name: Advanced Custom Fields: Mapbox GL Draw
Description: Add a custom field to place to draw points, lines or polygons on a map (based on 'Advanced Custom Fields: Mapbox GL JS' by WP Bees).
Version: 1.0.1
Author: L'Apòstrof
Author URI: https://apostrof.coop
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// check if class already exists
if ( ! class_exists( 'apos_acf_plugin_mapbox_gl_draw' ) ) {

	class apos_acf_plugin_mapbox_gl_draw {
		// vars
		var $settings;

		/**
		 *  __construct
		 *
		 *  This function will setup the class functionality
		 *
		 * @type    function
		 * @date    27/07/2018
		 * @since    1.0.0
		 *
		 * @param    void
		 *
		 * @return    void
		 */
		function __construct() {
			// settings
			// - these will be passed into the field class.
			$this->settings = array(
				'version' => '1.0.0',
				'url'     => plugin_dir_url( __FILE__ ),
				'path'    => plugin_dir_path( __FILE__ )
			);

			// include field depending on ACF version
			add_action( 'acf/include_field_types', array( $this, 'include_field' ) ); // v5
			add_action( 'acf/register_fields', array( $this, 'include_field' ) ); // v4
		}

		/**
		 *  include_field
		 *
		 *  This function will include the field type class
		 *
		 * @type    function
		 *
		 * @param int $version
		 *
		 * @date    27/07/2018
		 * @since    1.0.0
		 *
		 * @return    void
		 */
		function include_field( $version ) {
			if ( ! $version ) {
				$version = 4;
			}

			// include file depending on version number
			include_once( 'fields/class-apos-acf-field-mapbox-gl-draw-v' . $version . '.php' );
		}
	}

	// initialize
	new apos_acf_plugin_mapbox_gl_draw();
}